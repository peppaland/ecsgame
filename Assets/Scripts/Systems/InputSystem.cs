﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class InputSystem : SystemBase
{
    protected override void OnUpdate()
    {
        //store as variables because strings "horizontal" and "vertical" are not blittable types
        var moveHorizontal = Input.GetAxis("Horizontal");
        var moveVertical = Input.GetAxis("Vertical");


        Entities.WithAll<InputData>().
            ForEach((ref MoveData movement, ref InputData input) => {
                //set input component data
                input.inputH = moveHorizontal;
                input.inputV = moveVertical;

                //set move component data
                movement.targetDirection = new float3(input.inputH, 0, input.inputV);

        }).Schedule();
    }
}
