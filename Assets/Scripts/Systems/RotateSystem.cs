﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class RotateSystem : SystemBase
{
    protected override void OnUpdate()
    {

        Entities.WithAll<RotateData>().
            ForEach((ref Rotation rotation, in MoveData movement, in RotateData rotate) => {

                //check if input not zero
                if (!movement.targetDirection.Equals(float3.zero))
                {
                    //lerp current rotation to new input direction
                    quaternion targetRotation = quaternion.LookRotationSafe(movement.targetDirection, math.up());
                    rotation.Value = math.slerp(rotation.Value, targetRotation, rotate.rotateSpeed);
                }


        }).Schedule();
    }
}
